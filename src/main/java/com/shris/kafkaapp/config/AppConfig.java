package com.shris.kafkaapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import lombok.Data;

@Configuration
@Data
public class AppConfig {

  @Value(value = "${kafka.highLevel.topic}")
  private String highLevelTopicName;

  @Value(value = "${kafka.lowLevel.topic}")
  private String lowLevelTopicName;

  @Value(value = "${kafka.produce.highLevel.topic.data}")
  private boolean produceHighLevelTopicData;

  @Value(value = "${kafka.produce.lowLevel.topic.data}")
  private boolean produceLowLevelTopicData;

  @Value(value = "${highlevel.message.count}")
  private int highLevelMessageCount;

  @Value(value = "${lowlevel.message.count}")
  private int lowLevelMessageCount;

}
