package com.shris.kafkaapp.controller;

import java.util.Random;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shris.kafkaapp.constants.AppConstants;
import com.shris.kafkaapp.domain.ClientMessage;
import com.shris.kafkaapp.kafka.producer.KafkaDataProducer;

/**
 *
 * @author
 */
@RestController
public class MainController {

  @Autowired
  private KafkaDataProducer kafkaDataProducer;

  @Value(value = "${kafka.highLevel.topic}")
  private String kafkaHighLevelTopicName;

  @Value(value = "${kafka.lowLevel.topic}")
  private String kafkaLowLevelTopicName;

  @GetMapping(value = "/")
  public ClientMessage defaultPage() {
    ClientMessage clientMessage = new ClientMessage();
    clientMessage.setIsLoggedIn(false);
    clientMessage.setMessage(AppConstants.LOGIN_FIRST);
    return clientMessage;
  }

  @GetMapping(value = "/produceHighLevelTopicMessage")
  public ClientMessage produceHighLevelTopicMessage() {
    ClientMessage clientMessage = new ClientMessage();
    clientMessage.setIsLoggedIn(false);
    clientMessage.setMessage(AppConstants.LOGIN_FIRST);
    kafkaDataProducer.produceMessage(UUID.randomUUID().toString(), kafkaHighLevelTopicName);
    return clientMessage;
  }

  @GetMapping(value = "/produceLowLevelTopicMessage")
  public ClientMessage produceLowLevelTopicMessage() {
    ClientMessage clientMessage = new ClientMessage();
    clientMessage.setIsLoggedIn(false);
    clientMessage.setMessage(AppConstants.LOGIN_FIRST);
    kafkaDataProducer.produceMessage("" + generateNumber(), kafkaLowLevelTopicName);
    return clientMessage;
  }

  public int generateNumber() {
    Random r = new Random(System.currentTimeMillis());
    return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
  }
}
