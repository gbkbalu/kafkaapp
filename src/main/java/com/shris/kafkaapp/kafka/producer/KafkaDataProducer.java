package com.shris.kafkaapp.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
@Configuration
public class KafkaDataProducer {

  @Value(value = "${kafka.highLevel.topic}")
  private String kafkaHighLevelTopicName;

  @Value(value = "${kafka.lowLevel.topic}")
  private String kafkaLowLevelTopicName;

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  public void produceMessage(String jsonProduceMsg, String topicName) {
    kafkaTemplate.send(topicName, jsonProduceMsg);
  }

}
