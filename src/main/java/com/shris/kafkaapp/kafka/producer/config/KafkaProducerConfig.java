package com.shris.kafkaapp.kafka.producer.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

/**
 * Configuration class to send messages to Kafka.
 * 
 * Each Producer working with spring data needs have two things configured.
 * 
 * 1. ProducerFactory 2. KafkaTemplate
 * 
 * Each Kafka message is a {Key, Value} pair.
 * 
 * Each Producer needs to tell Kafka type of Key and Value that it is trying to
 * produce. This is part of ProducerFactory definition. As part of factory one
 * needs to configure where the Kafka is running and how key and values are
 * serialized.
 * 
 * @author Shris Infotech
 *
 */
// Telling Spring that we are going to declare few beans here.
@Configuration
public class KafkaProducerConfig {

	@Value(value = "${kafka.bootstrapAddress}")
	private String bootstrapAddress;

	/**
	 * Creates ProducerFactory Bean to send messages whose Key and Values are of
	 * type String.
	 * 
	 * @return ProducerFactory
	 */
	@Bean
	public ProducerFactory<String, String> producerFactory() {
		Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	/**
	 * Creates KafkaTemplate bean. This is the class on which one calls methods
	 * to send messages of type <String,String> to Kafka.
	 * 
	 * @return
	 */
	@Bean
	public KafkaTemplate<String, String> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}
}
