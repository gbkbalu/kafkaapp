package com.shris.kafkaapp.kafka.consumer;

import java.util.concurrent.CountDownLatch;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

public class KafkaConsumer {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  private CountDownLatch latch = new CountDownLatch(1);

  //@KafkaListener(topics = "${kafka.rawquote.topics}")
  @KafkaListener(topics = "#{'${spring.kafka.topics}'.split(',')}")
  public void consumeMessages(ConsumerRecord<String, String> record, Consumer<String, String> consumer)
      throws Exception {
    //System.out.println(record.topic()+"::"+record.value());
    logger.error(record.topic()+"::"+record.value());
    latch.countDown();
  }
}
