package com.shris.kafkaapp.service;

import java.util.Random;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shris.kafkaapp.config.AppConfig;
import com.shris.kafkaapp.kafka.producer.KafkaDataProducer;

@Service
public class KafkaInitDataService {

  @Autowired
  private KafkaDataProducer kafkaDataProducer;
  
  @Autowired
  private AppConfig appConfig;
  
  @PostConstruct
  public void produceHighLevelTopicMessages() {
    if(appConfig.isProduceHighLevelTopicData()) {
      for(int repeatCount=0;repeatCount<appConfig.getHighLevelMessageCount();repeatCount++) {
        String generatedUUID = UUID.randomUUID().toString();
        System.out.println(appConfig.getHighLevelTopicName()+"::"+generatedUUID);
        kafkaDataProducer.produceMessage(generatedUUID, appConfig.getHighLevelTopicName());
      }
    }
  }
  
  
  @PostConstruct
  public void produceLowLevelTopicMessages() {
    if(appConfig.isProduceLowLevelTopicData()) {
      for(int repeatCount=0;repeatCount<appConfig.getLowLevelMessageCount();repeatCount++) {
        System.out.println(appConfig.getLowLevelTopicName()+"::"+generateNumber());
        int randomNumber = generateNumber();
        kafkaDataProducer.produceMessage("" + randomNumber, appConfig.getLowLevelTopicName());
      }
    }
  }

  public int generateNumber() {
    Random r = new Random(System.currentTimeMillis());
    return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
  }

}
